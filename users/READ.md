#Sécurité
###Objectif
L'objectif de cette démonstration est de déployer un serveur centralisé de logs connecté à des applications.
Pour cette démonstration, toutes les applications seront conteneurisées.
###Configuration nécessaire pour la démonstration
Installation de maven
```commande
sudo apt install maven
```
Installation de virtualbox
```commande
sudo apt install virtualbox
```
Installation de vagrant et du plugin docker-compose
```commande
sudo apt install vagrant
vagrant plugin install vagrant-docker-compose
```
###Déploiement de l'application users
Se positionner dans le dossiers users et créer la VM :
```commande
cd /../users
mvn clean
mvn install -DskipTests
vagrant up
```
Se connecter à la VM
```commande
vagrant ssh api-users
```
Visualiser les applications déployées (une fois connecté dans le VM)
```commande
docker ps
```
###Déploiement de l'application product
Se positionner dans le dossier users et créer la VM :
```commande
cd /../product
mvn clean
mvn install -DskipTests
vagrant up
```
Se connecter à la VM
```commande
vagrant ssh api-product
```
Visualiser les applications déployées (une fois connecté dans le VM)
```commande
docker ps
```
###Pour intéragir avec les applications
API users
```commande
#Tous les users
192.168.50.22:8080/user
#User by id
192.168.50.22:8080/users/{id}
```
API product
```commande
#Tous les produits
192.168.50.22:8180/user
#User by id
192.168.50.22:8180/users/{id}
```
Suite ELK
```commande
#Accès à Kibana
192.168.50.25:5601
```
NB : Nous avons mis les IP pour bien montrer que chaque service peut-être déployé sur une machine différente et que la centralisation fonctionne bien. Les VM étant configurées pour faire foward les ports, on peut tout de même accéder à chacun de ces services en localhost en reprenant les ports énumérés.
