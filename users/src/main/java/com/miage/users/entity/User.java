package com.miage.users.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "users", schema = "bdd_users")
@Setter
@Getter
public class User {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String nom;
    private String prenom;
    private int age;

    public User(String nom, String prenom, int age) {
        this.age=age;
        this.nom=nom;
        this.prenom=prenom;
    }

    public User() {

    }
}
