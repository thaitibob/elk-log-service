package com.miage.users.controller;

import com.miage.users.entity.User;
import com.miage.users.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(value="/user", produces= MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    private UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        logger.info("Récupération de tous les users");
        Iterable<User> all = userRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{userId}")
    public ResponseEntity<?> getUser(@PathVariable("userId") int id) {
        logger.warn("Récupération d'un user'");
        Optional<User> user = userRepository.findById(id);
        return user.isEmpty()
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(user);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> addUser(@RequestBody User user) {
        User i = new User(user.getNom(), user.getPrenom(), user.getAge());
        User save = userRepository.save(i);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> updateUser(@RequestBody User user, @PathVariable("userId") int userId) {
        Optional<User> body = Optional.ofNullable(user);
        if (body.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        if (!userRepository.existsById(userId)) {
            return ResponseEntity.notFound().build();
        }
        user.setId(userId);
        User result = userRepository.save(user);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{userId}")
    @Transactional
    public ResponseEntity<?> deleteUser(@PathVariable("userId") int clientId) {
        Optional<User> user = userRepository.findById(clientId);
        if (user.isPresent()) {
            userRepository.delete(user.get());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
