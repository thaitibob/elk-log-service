package com.miage.product.controller;


import com.miage.product.entity.Product;
import com.miage.product.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.Optional;

@RestController
@RequestMapping(value="/product", produces= MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    private ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<?> getAllProducts() {
        logger.info("Récupération de tous les products");
        Iterable<Product> all = productRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping(path="/{productId}")
    public ResponseEntity<?> getProduct(@PathVariable("productId") int id) {
        logger.warn("Récupération d'un product'");
        Optional<Product> product = productRepository.findById(id);
        return product.isEmpty()
                ? ResponseEntity.notFound().build()
                : ResponseEntity.ok(product);
    }

    @PostMapping
    @Transactional
    public ResponseEntity<?> addProduct(@RequestBody Product product) {
        Product i = new Product(product.getLibelle(), product.getPrix());
        Product save = productRepository.save(i);
        return new ResponseEntity<>(save, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{productId}")
    @Transactional
    public ResponseEntity<?> updateProduct(@RequestBody Product product, @PathVariable("productId") int productId) {
        Optional<Product> body = Optional.ofNullable(product);
        if (body.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        if (!productRepository.existsById(productId)) {
            return ResponseEntity.notFound().build();
        }
        product.setId(productId);
        Product result = productRepository.save(product);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{productId}")
    @Transactional
    public ResponseEntity<?> deleteProduct(@PathVariable("productId") int clientId) {
        Optional<Product> product = productRepository.findById(clientId);
        if (product.isPresent()) {
            productRepository.delete(product.get());
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
