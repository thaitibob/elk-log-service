package com.miage.product.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "products", schema = "bdd_products")
@Setter
@Getter
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    private String libelle;
    private int prix;

    public Product(String libelle, int prix) {
        this.libelle = libelle;
        this.prix = prix;
    }


    public Product() {

    }
}
